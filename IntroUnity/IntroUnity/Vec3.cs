﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroUnity
{
    class Vec3
    {
        private double _x, _y, _z;

        public Vec3() { }
        public Vec3(double x, double y, double z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        public double X { get => _x; set => _x = value; }
        public double Y { get => _y; set => _y = value; }
        public double Z { get => _z; set => _z = value; }

        public void Add(Vec3 other) {
            X += other.X;
            Y += other.Y;
            Z += other.Z;
        }

        public void Substract(Vec3 other)
        {
            X -= other.X;
            Y -= other.Y;
            Z -= other.Z;
        }

        public void Multiply(int scalar)
        {
            X *= scalar;
            Y *= scalar;
            Z *= scalar;
        }

        public float Magnitude() {
            float magnitud = (float)Math.Sqrt(Math.Pow(X,2)+Math.Pow(Y,2)+Math.Pow(Z,2));
            return magnitud;
        }

        public void Normalize() {
            X /= Magnitude();
            Y /= Magnitude();
            Z /= Magnitude();
        }

        public Vec3 GetNormalized() {
            Vec3 result = new Vec3(X, Y, Z);
            result.Normalize();
            return result;
        }

        /*
         *          |i   j  k|      |Aj Ak|     |Ai Ak|     |Ai Aj|
         * AxB =    |Ai Aj Ak|  =   |Bj Bk| i - |Bi Bk| j + |Bi Bj| k = 
         *          |Bi Bj Bk|
         *          
         *          = [(Aj*Bk)-(Ak*Bj)] i - [(Ai*Bk)-(Ak*Bi)] j + [(Ai*Bj)-(Aj*Bi)] k
         */
        public Vec3 CrossProduct(Vec3 other) {
            Vec3 result = new Vec3();
            result.X = (Y * other.Z) - (Z * other.Y);
            result.Y = - ((X * other.Z) - (Z * other.X));
            result.Z = (X * other.Y) - (Y * other.X);
            return result;
        }

        /*
         * A·B = (Ax·Bx)+(Ay·By)+(Az·Bz)
         */

        public float DotProduct(Vec3 other) {
            float result = (float)((X * other.X) + (Y * other.Y) + (Z * other.Z));
            return result;
        }
        
        public static Vec3 operator + (Vec3 v1, Vec3 v2)
        {
            Vec3 v1aux = new Vec3(v1.X, v1.Y, v1.Z);
            v1aux.Add(v2);
            return v1aux;
        }
        
        public static Vec3 operator - (Vec3 v1, Vec3 v2)
        {
            Vec3 v1aux = new Vec3(v1.X, v1.Y, v1.Z);
            v1aux.Substract(v2);
            return v1aux;
        }

        public static Vec3 operator * (Vec3 v1, Vec3 v2)
        {
            Vec3 v1aux = new Vec3(v1.X, v1.Y, v1.Z);
            v1aux.CrossProduct(v2);
            return v1aux;
        }

        /*
         * ¿¿Overload de += y -= ??
        */
    }
}
