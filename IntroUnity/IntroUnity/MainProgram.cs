﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroUnity
{
    class MainProgram
    {
        static void Main(string[] args)
        {
            Vec3 v1 = new Vec3(-3, -2, 5);
            Vec3 v2 = new Vec3(6, -10, -1);

            Console.WriteLine("{0} {1} {2}", v1.X, v1.Y, v1.Z);
            Console.WriteLine("{0} {1} {2}", v2.X, v2.Y, v2.Z);
            Console.WriteLine("{0} {1} {2}", v1.CrossProduct(v2).X, v1.CrossProduct(v2).Y, v1.CrossProduct(v2).Z);
            Vec3 res = v1 + v2;
            Console.WriteLine("Suma {0} {1} {2}", res.X, res.Y, res.Z);
            Console.WriteLine("v1 {0} {1} {2}", v1.X, v1.Y, v1.Z);
            v1 += v2;
            Console.WriteLine("v1 {0} {1} {2}", v1.X, v1.Y, v1.Z);
            Console.WriteLine();
            Console.WriteLine("GetNormalized {0} {1} {2}", v1.GetNormalized().X, v1.GetNormalized().Y, v1.GetNormalized().Z);
            Console.WriteLine("v1 {0} {1} {2}", v1.X, v1.Y, v1.Z);
            v1.Normalize();
            Console.WriteLine("after normalizing v1 {0} {1} {2}", v1.X, v1.Y, v1.Z);

            Console.ReadKey();
        }
    }
}
