﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramacionCsharp
{
    class Ejemplos
    {
        // Función HolaMundo
        static void helloWorld()
        {
            Console.WriteLine("Hola Mundo!");
        }

        // Comprueba si un número es perfecto y devuelve verdadero o falso
        static void isPerfectNum()
        {
            Console.WriteLine("Introduce un número para comprobar si es perfecto:");
            int n = Convert.ToInt16(Console.ReadLine());
            int divsum = 0;
            for (int i = 1; i < n; i++)
            {
                if (n % i == 0)
                {
                    divsum += i;
                }
            }

            if (divsum == n)
            {
                Console.WriteLine("El número introducido SÍ es perfecto");
            }
            else
            {
                Console.WriteLine("El número introducido NO es perfecto");
            }
        }

        // Busca números perfectos hasta el número 10.000
        static void allPerfectNum()
        {
            int nums = 10000;
            Console.WriteLine("Números perfectos entre el 1 y el {0}", nums);
            for (int perfectnum = 1; perfectnum < nums; perfectnum++)
            {
                int divsum = 0;
                for (int i = 1; i < perfectnum; i++)
                {
                    if (perfectnum % i == 0)
                    {
                        divsum += i;
                    }

                }
                if (divsum == perfectnum)
                {
                    Console.WriteLine("El número {0} es perfecto", perfectnum);
                }
            }
        }

        // Calcula el factorial de un número
        static void factorial()
        {
            Console.WriteLine("Introduce un número para calcular su factorial:");
            int num = Convert.ToInt16(Console.ReadLine());
            int orignum = num;
            int fact = 1;
            for (; num > 1; num--)
            {
                fact = fact * num;
            }
            Console.WriteLine("El factorial de {0} es {1}", orignum, fact);
        }

        // Crea una lista de números pares
        static void pairNumbers()
        {
            int[] pares = new int[10];
            for (int i = 0; i < 9; i++)
            {
                pares[i] = 2 * i;
                Console.WriteLine("Número {0} introducido en la posición {1}", pares[i], i);
            }
        }

        // Calcula el resto de una división de dos números
        static int restDivision(int dividendo, int divisor)
        {
            int c = 0;
            int origdividendo = dividendo;
            while (dividendo >= divisor)
            {
                dividendo = dividendo - divisor;
                c = c + 1;
            }

            Console.WriteLine("El resto de dividir {0} entre {1} es {2}", origdividendo, divisor, dividendo);

            return dividendo;
        }

        // Muestra el menor de dos números
        static void mostrarMenorDe(int i, int j)
        {
            Console.WriteLine("El número menor entre {0} y {1} es {2}", i, j, i < j ? i : j);
        }

        // Encuentra y muestra el mínimo valor de una lista de números
        static void mostrarMenorDeLista()
        {
            int[] lista = new int[5];
            lista[0] = 15;
            lista[1] = 2;
            lista[2] = 7;
            lista[3] = 3;
            lista[4] = 11;
            int menor = lista[0];
            Console.WriteLine("Lista: {0} {1} {2} {3} {4}", lista[0], lista[1], lista[2], lista[3], lista[4]);
            for (int i = 0; i < 5; i++)
            {
                if (lista[i] < menor)
                {
                    menor = lista[i];
                }
            }
            Console.WriteLine("El menor valor de la lista es {0}", menor);
        }

        // Duplica los valores de una lista
        static void duplicaLista()
        {
            int[] lista = new int[3];
            lista[0] = 1;
            lista[1] = 2;
            lista[2] = 7;
            Console.WriteLine("Valores originales de la lista: {0} {1} {2}", lista[0], lista[1], lista[2]);
            for (int i = 1; i < 3; i++) {
                lista[i] = lista[i] * 2;
            }
            Console.WriteLine("Valores duplicados de la lista: {0} {1} {2}", lista[0], lista[1], lista[2]);
        }

        // Suma los valores de dos listas de mismas dimensiones
        static void sumaLista()
        {
            int[] listaA = new int[3];
            int[] listaB = new int[3];
            int[] listaAmasB = new int[3];
            listaA[0] = 1;
            listaA[1] = 2;
            listaA[2] = 7;
            listaB[0] = 2;
            listaB[1] = 7;
            listaB[2] = 3;
            for (int i = 0; i < 3; i++)
            {
                listaAmasB[i] = listaA[i] + listaB[i];
            }
            Console.WriteLine("Lista A: \t {0} {1} {2}", listaA[0], listaA[1], listaA[2]);
            Console.WriteLine("Lista B: \t {0} {1} {2}", listaB[0], listaB[1], listaB[2]);
            Console.WriteLine("Lista A+B: \t {0} {1} {2}", listaAmasB[0], listaAmasB[1], listaAmasB[2]);
        }

        // Impresión estética por pantalla entre ejecución de funciones
        static void nextStep()
        {
            Console.WriteLine();
            Console.WriteLine("---------");
            Console.WriteLine("Pulsa cualquier tecla para continuar...");
            Console.WriteLine("---------");
            Console.WriteLine();
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            helloWorld();

            nextStep();

            isPerfectNum();

            nextStep();

            allPerfectNum();

            nextStep();

            factorial();

            nextStep();

            pairNumbers();

            nextStep();

            restDivision(25, 7);

            nextStep();

            mostrarMenorDe(6, 7);

            nextStep();

            mostrarMenorDeLista();

            nextStep();

            duplicaLista();

            nextStep();

            sumaLista();

            nextStep();
        }
    }
}
